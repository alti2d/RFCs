# Altitude RFCs

Substantial changes to altitude should go through an informal public design
process -- the RFC (Request For Comment) process provides a consistent path
for discussing and refining these changes, so everyone can be confident about
the direction we're moving in.

The issues section can be used to discuss ideas before there's any concrete
proposal.

RFCs are submitted through a pull request, they should follow the template at
the root of the repository. While the request is open, anyone can comment in the
comment thread of the pull request with their thoughts, concerns, alternative, etc.

An RFC being accepted is no indication of the priority of it's implementation,
just that in principle everyone has agreed to the feature and are amenable to
it's implementation.

## Contributions

All contributions submitted for inclusion to this repository shall be licensed
under the MIT license (http://opensource.org/licenses/MIT).

