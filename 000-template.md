# Feature Name

A single paragraph explaining the feature.

# Motivation

Why do we want to add this?

# Detailed Explanation

A detailed proposal of the change, how it interacts with other features, and
how it would be implemented.

# Drawbacks

The disadvantages of the change.

# Alternatives

A list of possible alternatives. What's the impact of not doing this?

